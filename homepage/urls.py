from django.urls import path
from homepage import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('login',  views.LogIn, name="login"),
    path('logout', views.LogOut, name="logout"),
    path('register', views.Register, name='register')
]
